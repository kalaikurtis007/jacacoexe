#!/bin/bash
mkdir opensearch
cd opensearch
echo "going to download opensearch"
wget https://artifacts.opensearch.org/releases/bundle/opensearch/2.3.0/opensearch-2.3.0-linux-x64.tar.gz
tar -xvf opensearch-2.3.0-linux-x64.tar.gz
echo "going to download dashboard"
wget https://artifacts.opensearch.org/releases/bundle/opensearch-dashboards/2.3.0/opensearch-dashboards-2.3.0-linux-x64.tar.gz
tar -xvf opensearch-dashboards-2.3.0-linux-x64.tar.gz
./opensearch-2.3.0/bin/opensearch
./opensearch-dashboards-2.3.0/bin/opensearch-dashboards 


